﻿using MazeSolver.Algorithms;
using MazeSolver.Models;
using MazeSolver.Utilities;
using System.Diagnostics;
using System.Drawing;

/*
Console.WriteLine("\nEnter the maze file directory (relative path and name):");
string inputPath = Console.ReadLine();

Console.ForegroundColor = ConsoleColor.Green;
Console.WriteLine("\nReading maze");
*/

//var path = new Uri(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)).LocalPath;

Stopwatch stopwatch = new();
stopwatch.Start();

string path = @"C:\Users\Utente\Documents\Test\MazeSolver\MazeSolver.Tests\Examples\";
Bitmap original = new($"{path}MultipleSolutions_DeadEnds_Large.png", true);

Console.WriteLine("Creating maze");
var maze = new Maze(original);

Console.WriteLine($"Node count: {maze.NodeCount}");
Console.WriteLine($"Time elapsed: {stopwatch.ElapsedMilliseconds} [ms]");
stopwatch.Restart();

List<Node> result;

result = Methods.DepthFirstSearch(maze.Start, maze.End);
Console.WriteLine($"Depth First Search - Shortest path count: {Utility.StepsBetweenNodes(result)}");
Console.WriteLine($"Time elapsed: {stopwatch.ElapsedMilliseconds} [ms]");
ImageManipulation.CopyWithSolution(original, result, path, "_DepthFirstSearch");
stopwatch.Restart();

result = Methods.BreadthFirstSearch(maze.Start, maze.End);
Console.WriteLine($"Breadth First Search - Shortest path count: {Utility.StepsBetweenNodes(result)}");
Console.WriteLine($"Time elapsed: {stopwatch.ElapsedMilliseconds} [ms]");
ImageManipulation.CopyWithSolution(original, result, path, "_BreadthFirstSearch");
stopwatch.Restart();

result = Methods.Dijkastra(maze.Start, maze.End);
Console.WriteLine($"Dijkastra - Shortest path count: {Utility.StepsBetweenNodes(result)}");
Console.WriteLine($"Time elapsed: {stopwatch.ElapsedMilliseconds} [ms]");
ImageManipulation.CopyWithSolution(original, result, path, "_Dijkastra");
stopwatch.Restart();

result = Methods.AStar(maze.Start, maze.End);
Console.WriteLine($"A* - Shortest path count: {Utility.StepsBetweenNodes(result)}");
Console.WriteLine($"Time elapsed: {stopwatch.ElapsedMilliseconds} [ms]");
ImageManipulation.CopyWithSolution(original, result, path, "_AStar");
stopwatch.Restart();


stopwatch.Stop();
