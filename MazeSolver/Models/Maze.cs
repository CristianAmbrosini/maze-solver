﻿using System.Drawing;

namespace MazeSolver.Models {
    public class Maze {

        public int Width;
        public int Height;

        public int NodeCount = 0;

        public Node Start;
        public Node End;

        public Bitmap Grid;

        public Maze(Bitmap image) {
            Grid = image;
            Width = image.Width;
            Height = image.Height;

            Start = SearchEmptyPixelInRow(0);
            End = SearchEmptyPixelInRow(Height - 1);
            NodeCount += 2;

            AdjacencyList();
        }

        private void AdjacencyList() {
            Node[] topNodes = new Node[Width];
            for (int i = 0; i < topNodes.Length; i++)
                topNodes[i] = null;
            topNodes[Start.X] = Start;

            for (int y = 1; y < Height - 1; y++) {
                bool prev;
                bool curr = false;
                bool next = IsPath(1, y);
                Node leftNode = null;

                for (int x = 1; x < Width - 1; x++) {
                    prev = curr;
                    curr = next;
                    next = IsPath(x + 1, y);
                    Node node = null;
                    if (!curr)
                        continue;

                    if (prev) {
                        if (next) {
                            // 1,1,1 - create only if path above or below
                            if (IsPath(x, y + 1) || IsPath(x, y - 1)) {
                                node = new Node(x, y);
                                leftNode.Neighbours[1] = node;
                                node.Neighbours[3] = leftNode;
                                leftNode = node;
                            }
                        }
                        else {
                            // 1,1,0 - end of the corridor
                            node = new Node(x, y);
                            leftNode.Neighbours[1] = node;
                            node.Neighbours[3] = leftNode;
                            leftNode = null;
                        }
                    }
                    else {
                        if (next) {
                            // 0,1,1 - start of the corridor
                            node = new Node(x, y);
                            leftNode = node;
                        }
                        else {
                            // 0,1,0 - create only if it is a dead end
                            if (!IsPath(x, y + 1) || !IsPath(x, y - 1))
                                node = new Node(x, y);
                        }
                    }

                    if (node != null) {
                        NodeCount++;
                        // If there is a path above search and link pendent node;
                        if (IsPath(x, y - 1)) {
                            Node top = topNodes[node.X];
                            node.Neighbours[0] = top;
                            top.Neighbours[2] = node;
                        }
                        topNodes[node.X] = IsPath(x, y + 1) ? node : null;
                    }
                }
            }
            Node secondLast = topNodes[End.X];
            End.Neighbours[0] = secondLast;
            secondLast.Neighbours[2] = End;
        }

        private Node SearchEmptyPixelInRow(int y) {
            if (y < 0 || y >= Height)
                throw new ArgumentOutOfRangeException($"The row {y} is outside the image.");

            for (int x = 0; x < Width; x++)
                if (IsPath(x, y))
                    return new Node(x, y);
            throw new Exception($"No empty pixel in row {y} was found");
        }

        private bool IsPath(int x, int y) {
            if (x < 0 || x >= Width || y < 0 || y >= Height)
                throw new ArgumentOutOfRangeException($"The pixel on {x}, {y} is outside the image.");
            return Grid.GetPixel(x, y).ToArgb().Equals(Color.White.ToArgb());
        }
    }
}

