﻿
namespace MazeSolver.Models {
    public class Node {
        public int X { get; set; }
        public int Y { get; set; }
        public Node[] Neighbours { get; set; } = new Node[4]; // Up/Right/Down/Left

        public Node(int x, int y) { 
            X = x;
            Y = y;
        }
    }
}
