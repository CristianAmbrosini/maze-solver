﻿using MazeSolver.Models;
using System.Drawing;
using System.Drawing.Imaging;

namespace MazeSolver.Utilities {
    public static class ImageManipulation {
        public static void CopyWithSolution(Bitmap original, List<Node> result, string path, string name) {
            if (!Directory.Exists(path))
                throw new Exception("Directory not found.");
            using Bitmap finishMaze = new(original);
            for (int i = 0; i < result.Count - 1; i++) {
                Node nodeA = result[i];
                Node nodeB = result[i + 1];
                // Horizontal line
                if (nodeA.Y == nodeB.Y) {
                    for (int x = Math.Min(nodeA.X, nodeB.X); x <= Math.Max(nodeA.X, nodeB.X); x++)
                        finishMaze.SetPixel(x, nodeA.Y, Color.Red);
                }
                // Vertical line
                else if (nodeA.X == nodeB.X) {
                    for (int y = Math.Min(nodeA.Y, nodeB.Y); y <= Math.Max(nodeA.Y, nodeB.Y); y++)
                        finishMaze.SetPixel(nodeA.X, y, Color.Red);
                }
            }
            finishMaze.Save($@"{path}\{name}.png", ImageFormat.Png);
        }
    }
}
