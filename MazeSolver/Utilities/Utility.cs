﻿using MazeSolver.Models;

namespace MazeSolver.Utilities {
    public static class Utility {
        public static int ManhattanDistance(int x1, int y1, int x2, int y2) => Math.Abs(x1 - x2) + Math.Abs(y1 - y2);

        public static int StepsBetweenNodes(List<Node> result) {
            int count = 0;
            for (int i = 0; i < result.Count - 1; i++) {
                Node nodeA = result[i];
                Node nodeB = result[i + 1];
                // Horizontal line
                if (nodeA.Y == nodeB.Y) {
                    for (int x = Math.Min(nodeA.X, nodeB.X); x < Math.Max(nodeA.X, nodeB.X); x++)
                        count++;
                }
                // Vertical line
                else if (nodeA.X == nodeB.X) {
                    for (int y = Math.Min(nodeA.Y, nodeB.Y); y < Math.Max(nodeA.Y, nodeB.Y); y++)
                        count++;
                }
            }
            return count;
        }
    }
}
