﻿using MazeSolver.Models;

namespace MazeSolver.Algorithms {
    public static partial class Methods {
        public static List<Node> DepthFirstSearch(Node start, Node end) {
            Dictionary<Node, Node> dict = new();

            Stack<Node> stack = new();
            stack.Push(start);
            Node current;
            while (stack.Any()) {
                current = stack.Pop();
                if (current == end)
                    break;

                foreach (Node neighbor in current.Neighbours) {
                    if (neighbor == null || dict.ContainsKey(neighbor))
                        continue;
                    dict.Add(neighbor, current);
                    stack.Push(neighbor);
                }
            }

            return GenerateList();

            List<Node> GenerateList() {
                List<Node> list = new();
                if (!dict.ContainsKey(end))
                    return list;

                current = end;
                while (current != null && current != start) {
                    list.Insert(0, current);
                    current = dict[current];
                }
                list.Insert(0, current);
                return list;
            }
        }
    }
}
