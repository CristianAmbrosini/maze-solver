﻿using MazeSolver.Models;

namespace MazeSolver.Algorithms {
    public static partial class Methods {
        public static List<Node> BreadthFirstSearch(Node start, Node end) {
            Dictionary<Node, Node> dict = new();

            Queue<Node> queue = new();
            queue.Enqueue(start);
            Node current;
            while (queue.Any()) {
                current = queue.Dequeue();
                if (current == end)
                    break;

                foreach(Node neighbor in current.Neighbours) {
                    if (neighbor == null || dict.ContainsKey(neighbor))
                        continue;
                    dict.Add(neighbor, current);
                    queue.Enqueue(neighbor);
                }
            }

            return GenerateList();

            List<Node> GenerateList() {
                List<Node> list = new();
                if (!dict.ContainsKey(end))
                    return list;

                current = end;
                while (current != null && current != start) {
                    list.Insert(0, current);
                    current = dict[current];
                }
                list.Insert(0, current);
                return list;
            }
        }
    }
}
