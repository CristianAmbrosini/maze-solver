﻿using MazeSolver.Models;
using MazeSolver.Utilities;
using MazeSolver.DataStructures;

namespace MazeSolver.Algorithms {
    public static partial class Methods {

        public static List<Node> AStar(Node start, Node end) {
            HashSet<Node> visited = new();
            Dictionary<Node, Node> parent = new();

            Dictionary<Node, int> distances = new();

            Dictionary<Node, FibonacciHeapNode<Node, int>> fibHeapSet = new();
            FibonacciHeap<Node, int> fibHeap = new(0);

            distances[start] = 0;
            Insert(start, 0);

            Node current;
            while (!fibHeap.IsEmpty()) {
                current = fibHeap.RemoveMin().Data;
                if (!distances.ContainsKey(current))
                    break;
                if (current == end) // Finish
                    break;
                foreach (Node neighbor in current.Neighbours) {
                    if (neighbor == null || visited.Contains(neighbor))
                        continue;
                    int distance = Utility.ManhattanDistance(current.X, current.Y, neighbor.X, neighbor.Y);
                    int newDistance = distances[current] + distance;
                    int remaining = Utility.ManhattanDistance(neighbor.X, neighbor.Y, end.X, end.Y);
                    if (!distances.ContainsKey(neighbor) || newDistance < distances[neighbor]) {
                        if (!fibHeapSet.ContainsKey(neighbor)) {
                            Insert(neighbor, newDistance + remaining);
                            distances[neighbor] = newDistance;
                        }
                        else if ((newDistance + remaining).CompareTo(fibHeapSet[neighbor].Key) < 0)
                            fibHeap.DecreaseKey(fibHeapSet[neighbor], newDistance + remaining);
                        parent[neighbor] = current;
                    }
                }
                visited.Add(current);
            }

            return GenerateList();

            void Insert(Node node, int distance) {
                FibonacciHeapNode<Node, int> toAdd = new(node, distance);
                fibHeap.Insert(toAdd);
                fibHeapSet.Add(node, toAdd);
            }

            List<Node> GenerateList() {
                List<Node> list = new();
                if (!parent.ContainsKey(end))
                    return list;

                current = end;
                while (current != null && current != start) {
                    list.Insert(0, current);
                    current = parent[current];
                }
                list.Insert(0, current);
                return list;
            }
        }
    }
}
