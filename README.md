[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Maze solver

> A maze is a path or collection of paths, typically from an entrance to a goal. The word is used to refer both to branching tour puzzles through which the solver must find a route, and to simpler non-branching ("unicursal") patterns that lead unambiguously through a convoluted layout to a goal. - Wikipedia

## Solver

This appplication will solve mazes from an input image returning the same image with the *optimal path* highlighted.
This project use maze-solving algorithms as an automated method for solving each maze, you can try different algorithms and compare **space** and **time** complexities.
The maze should be sourced as a non compressed, black & white image where each pixel represents a path if it's white, and a wall if it's black. The starting and ending point will be placed respectively at the top and at the bottom.

Fload fill approach will results in memory overhead so the logic proposed here takes a step further.
The code first scan through each pixel image and through a logic it create nodes in interesting crosses or path skipping all the empty corridor and linking them to the 4 neighbors in each direction.
The output graph will be processed by **different algorithms** to analyze their results:
- **Depth First Search**
- **Breadth First Search**
- **Dijkastra**
- **A Star**

Below an example of the ouput of these alghoritm in action with a medium sized maze.
The total execution time of the first pass to detect and create nodes takes in this PC 221 ms, returning a graph of 6309 nodes.

#### Depth First Search
Execution Time: **4[ms]**<br/>
N° of steps to escape: **3172**

![DepthFirstSearch](img/DepthFirstSearch.png)

#### Breadth First Search
Execution Time: **3[ms]**<br/>
N° of steps to escape: **620**

![BreadthFirstSearch](img/BreadthFirstSearch.png)

#### Dijkastra
Execution Time: **25[ms]**<br/>
N° of steps to escape: **596**

![Dijkastra](img/Dijkastra.png)

#### A*
Execution Time: **55[ms]**<br/>
N° of steps to escape: **596**

![AStar](img/AStar.png)

## License
MIT License

Copyright (c) 2022 Cristian Ambrosini

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
